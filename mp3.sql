-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jul 24, 2015 at 12:23 PM
-- Server version: 5.5.38-log
-- PHP Version: 5.6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `mp3`
--

-- --------------------------------------------------------

--
-- Table structure for table `baihat`
--

CREATE TABLE `baihat` (
`id` int(11) NOT NULL,
  `tenbaihat` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `casy` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `tacgia` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `theloai` int(10) NOT NULL,
  `duongdan` varchar(200) CHARACTER SET latin1 NOT NULL,
  `loibaihat` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `luotnghe` int(10) NOT NULL,
  `chude` int(10) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `baihat`
--

INSERT INTO `baihat` (`id`, `tenbaihat`, `casy`, `tacgia`, `theloai`, `duongdan`, `loibaihat`, `luotnghe`, `chude`, `status`, `created_at`) VALUES
(40, 'Because I Miss You', '1', '', 0, 'nhac/Dong Nhi - Bad Boy.mp3', '', 0, 3, NULL, '2015-07-16 04:06:01'),
(45, 'Gạt Đi Nước Mắt', '2', '', 0, 'nhac/Gat di nuoc mat.mp3', '', 0, 3, NULL, '2015-07-22 04:20:00'),
(46, 'Khuôn Mặt Đáng Thương', '1', '', 0, 'nhac/Khuon Mat Dang Thuong.mp3', '', 0, 3, NULL, '2015-07-15 09:05:26'),
(47, 'Thu Cuối', '3', '', 0, 'nhac/Thu Cuoi.mp3', '', 0, 3, NULL, '2015-07-24 06:03:26'),
(48, 'Because I Miss You', '2', '', 0, 'nhac/Because I Miss You - Jung Yong Hwa.mp3', '', 0, 3, NULL, '2015-07-11 08:09:00');

-- --------------------------------------------------------

--
-- Table structure for table `bangquyen`
--

CREATE TABLE `bangquyen` (
`id` int(11) NOT NULL,
  `noidung` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `casy`
--

CREATE TABLE `casy` (
`id` int(11) NOT NULL,
  `casy` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `casy`
--

INSERT INTO `casy` (`id`, `casy`) VALUES
(1, '365 Daband'),
(2, 'Đông Nhi'),
(3, 'Sơn Tùng M-TP');

-- --------------------------------------------------------

--
-- Table structure for table `chuchay`
--

CREATE TABLE `chuchay` (
  `id` int(11) NOT NULL,
  `noidung` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `chuchay`
--

INSERT INTO `chuchay` (`id`, `noidung`) VALUES
(1, 'Web nghe nhạc trực tuyền Online Mp3 | Dung Ham');

-- --------------------------------------------------------

--
-- Table structure for table `chude`
--

CREATE TABLE `chude` (
`id` int(11) NOT NULL,
  `chude` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `chude`
--

INSERT INTO `chude` (`id`, `chude`) VALUES
(12, 'Nhạc Việt Mới'),
(13, 'Nhạc Hot Rap Việt'),
(16, 'Love Songs'),
(17, 'Nhạc Sàn'),
(18, 'Nhạc Giáng Sinh');

-- --------------------------------------------------------

--
-- Table structure for table `footer`
--

CREATE TABLE `footer` (
  `id` int(10) NOT NULL,
  `noidung` varchar(9999) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `footer`
--

INSERT INTO `footer` (`id`, `noidung`) VALUES
(1, '<h1 style="font-family: Arial, Verdana; font-size: 10pt; font-weight: normal; text-align: center;"><hr></h1><h3 style="font-family: Arial, Verdana; font-size: 10pt; text-align: center;"><br></h3><blockquote style="margin: 0px 0px 0px 40px; border: none; padding: 0px;"><h1 style="font-family: Arial, Verdana; font-size: 10pt; text-align: center;"></h1><h4 style="text-align: center;"><font color="#ff0000" size="5" face="Courier New"><u>Miss Dung</u></font></h4><div style="font-family: Arial, Verdana; font-size: 10pt;"><font color="#ff0000"><u><br></u></font></div><div style="font-size: 10pt; text-align: center;"><font color="#ff0000" face="Comic Sans MS"><b><u>Liên hệ Facebook:</u></b>&nbsp;http://facebook.com/dung<b>&nbsp;<u>Điện Thoại:</u> </b>099999999</font></div></blockquote><hr style="font-family: Arial, Verdana; font-size: 10pt; font-weight: normal; font-style: normal; font-variant: normal; line-height: normal;">');

-- --------------------------------------------------------

--
-- Table structure for table `logo`
--

CREATE TABLE `logo` (
`id` int(11) NOT NULL,
  `noidung` varchar(500) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `logo`
--

INSERT INTO `logo` (`id`, `noidung`) VALUES
(1, 'images/logo/header.png');

-- --------------------------------------------------------

--
-- Table structure for table `luot`
--

CREATE TABLE `luot` (
  `luot` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `luot`
--

INSERT INTO `luot` (`luot`) VALUES
(1541);

-- --------------------------------------------------------

--
-- Table structure for table `status`
--

CREATE TABLE `status` (
`id` int(11) NOT NULL,
  `tinhtrang` varchar(10) CHARACTER SET latin1 NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `status`
--

INSERT INTO `status` (`id`, `tinhtrang`) VALUES
(1, 'OK'),
(2, 'NO');

-- --------------------------------------------------------

--
-- Table structure for table `theloai`
--

CREATE TABLE `theloai` (
`id` int(11) NOT NULL,
  `noidung` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `theloai`
--

INSERT INTO `theloai` (`id`, `noidung`) VALUES
(1, 'Nhạc Quốc Tế'),
(2, 'Việt Nam'),
(3, 'Âu Mỹ'),
(4, 'Hàn Quốc'),
(5, 'Rap Việt'),
(6, 'Cách Mạng');

-- --------------------------------------------------------

--
-- Table structure for table `timelog`
--

CREATE TABLE `timelog` (
`id` int(11) NOT NULL,
  `time` date NOT NULL,
  `day` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
`id` int(11) NOT NULL,
  `username` varchar(100) CHARACTER SET latin1 NOT NULL,
  `password` varchar(100) CHARACTER SET latin1 NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(20) CHARACTER SET latin1 NOT NULL DEFAULT 'OK',
  `gioitinh` varchar(4) CHARACTER SET latin1 NOT NULL,
  `ngaysinh` varchar(30) CHARACTER SET latin1 NOT NULL,
  `diachi` varchar(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) CHARACTER SET latin1 NOT NULL,
  `id_group` tinyint(3) NOT NULL DEFAULT '1',
  `permission` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `name`, `status`, `gioitinh`, `ngaysinh`, `diachi`, `email`, `id_group`, `permission`) VALUES
(1, 'admin', 'admin', 'Dung Ham', 'OK', '', '', 'RAH', 'dung@gmail.com', 1, 'nhacmoi,nhacdadang,theloai,nhachot');

-- --------------------------------------------------------

--
-- Table structure for table `user_group`
--

CREATE TABLE `user_group` (
`maQuyen` int(11) NOT NULL COMMENT 'Mã quyền',
  `tenQuyen` varchar(16) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Tên quyền'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='Bảng quyền';

--
-- Dumping data for table `user_group`
--

INSERT INTO `user_group` (`maQuyen`, `tenQuyen`) VALUES
(1, 'Quản trị'),
(2, 'Quản lý'),
(3, 'Người dùng'),
(4, 'Khách');

-- --------------------------------------------------------

--
-- Table structure for table `user_online`
--

CREATE TABLE `user_online` (
  `session` varchar(99) CHARACTER SET latin1 NOT NULL,
  `time` varchar(20) CHARACTER SET latin1 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_online`
--

INSERT INTO `user_online` (`session`, `time`) VALUES
('960ce6c29f3823bd4fd5d16611023b24', '1437733319');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `baihat`
--
ALTER TABLE `baihat`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bangquyen`
--
ALTER TABLE `bangquyen`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `casy`
--
ALTER TABLE `casy`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chuchay`
--
ALTER TABLE `chuchay`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chude`
--
ALTER TABLE `chude`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logo`
--
ALTER TABLE `logo`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status`
--
ALTER TABLE `status`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `theloai`
--
ALTER TABLE `theloai`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `timelog`
--
ALTER TABLE `timelog`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_group`
--
ALTER TABLE `user_group`
 ADD PRIMARY KEY (`maQuyen`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `baihat`
--
ALTER TABLE `baihat`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `bangquyen`
--
ALTER TABLE `bangquyen`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `casy`
--
ALTER TABLE `casy`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `chude`
--
ALTER TABLE `chude`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `logo`
--
ALTER TABLE `logo`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `status`
--
ALTER TABLE `status`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `theloai`
--
ALTER TABLE `theloai`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `timelog`
--
ALTER TABLE `timelog`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `user_group`
--
ALTER TABLE `user_group`
MODIFY `maQuyen` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Mã quyền',AUTO_INCREMENT=5;
DELIMITER $$
--
-- Events
--
CREATE DEFINER=`root`@`localhost` EVENT `resetCount` ON SCHEDULE EVERY 1 MINUTE STARTS '2015-07-23 00:00:00' ON COMPLETION NOT PRESERVE ENABLE DO UPDATE baihat SET luotnghe = 0$$

DELIMITER ;
