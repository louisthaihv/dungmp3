<?php
	$mod = $_GET['mod'];

	switch($mod)
	{
		case "nhacmoi":
			include("nhacmoi.php");
			break;
		case "nhacdadang":
			include("nhacdadang.php");
			break;
		case "theloai":
			include("theloai.php");
			break;
		case "nhachot":
			include("nhachot.php");
			break;
		case "upload":
			include("upload.php");
			break;
		case "chude":
			if($_SESSION['id_group'] == 1){
				include("chude.php");
			} 
			break;
		case "casy":
			if($_SESSION['id_group'] == 1){

				include("casy.php");
			}
			break;
		case "user":
			if($_SESSION['id_group'] == 1){

				include("user.php");
			}
			break;
		case "chuchay":
			if($_SESSION['id_group'] == 1){
				include("chuchay.php");
			}
			break;
		case "footer":
			if($_SESSION['id_group'] == 1){
				include("footer.php");
			}
			break;
		case "logo":
			if($_SESSION['id_group'] == 1){
				include("logo.php");
			}
			break;
		case "playadmin":
			include("playadmin.php");
			break;
		default: include("danhmuc.php");
	}
?>